// fetch() method is used to send request in the server and load the recieved response in the webpage. The request and response in in JSON format.

// Syntax:
	// fetch("url", options)
		// url - this is the url which the request is to be made (endpoint)
		// options - array of properties that contains the HTTP method, body of request, headers.

//Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
// invoke the showPosts()
.then((data)=> showPosts(data))


//Add post data.

document.querySelector("#form-add-post").addEventListener("submit", (e) =>{

	// Prevents the page from loading
	e.preventDefault();
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	body: JSON.stringify({
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value,
		userId: 1
	}),
	headers:{
		"Content-Type": "application/json"
	}
})
.then((response) => response.json())
.then((date) =>{
	console.log(data);
alert("Successfully added!");
})
	
// resets the state 
	
})

// View Posts
const showPosts = (posts) =>{
	//Create a variable that will contain all the posts
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post) =>{
		//onclick() is an event occurs when the user clicks on an element.
		// This allows us to execute a JavaScript's function when an element get clicked.
		// We can assign HTML elements in a JS variable.
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	//To check what is stored in the postEntries variables
	// console.log(postEntries);

	//To replace the content of the "div-post-entries"
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post Button
// We will create a function that will be called in the onclick() event and will pass the value in the Update Form input box.
const editPost = (id) =>{
	// Contain the value of the title and body in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body of the movie post to be updated in the Edit Post/Form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

// Update post.
// This will trigger an event that will update a post upon clicking the Update button.
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let postId = document.querySelector("#txt-edit-id").value
	let title = document.querySelector("#txt-edit-title").value
	let body = document.querySelector("#txt-edit-body").value

	console.log(postId);

	// Use a loop that will check each post stored in our mock database.
	for(i = 0; i < posts.length; i++){
		//Use a if statement to look for the post to be updated using id property.
		// The value posts[i].id is a Number while postId is a String, if we want to use the strictly equality operator (===) we need to convert the postId first.
		if(posts[i].id == postId){
			// reassign the value of the title and body property.
			posts[i].title = title;
			posts[i].body = body;

			// Invoke the showPosts() to check the updated posts.
			showPosts();

			alert("Successfully updated!");

			break;
		}
	}
})

//Activity Delete a post
const deletePost = (id) => {
	// filter method will save the posts that are not equal to the id parameter.
	posts = posts.filter((post) => {
		if(post.id != id){
			return post;
		}
	})

	console.log(posts);

	// .remove() method removes an element (or node) from the document.
	document.querySelector(`#post-${id}`).remove();
}
